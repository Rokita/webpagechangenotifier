import requests # To download the page
import time
import smtplib
import datetime
import json

import bs4 as bs

class WebPage: 
    webpageUrl = ""
    def __init__(self, url):
        self.webpageUrl = url
    
    def get_html_code(self):
        response = requests.get(self.webpageUrl)
        return response.text

class WebPageParser: 
    def __init__(self):
        pass

    def parse_lxml(self, pageHtml):
        return bs.BeautifulSoup(pageHtml, 'lxml')
    
    def get_web_title(self, pageHtml):
        soup = self.parse_lxml(pageHtml)
        return soup.title.string

    def get_http_links(self, pageHtml):
        soup = self.parse_lxml(pageHtml)
        tab = []
        for html_a in soup.find_all('a'):
            http_link = str(html_a.get("href")) 
            if http_link.startswith("http"):
                tab.append(http_link)
        return tab
        

class EmailNotifier:
    def sendNotification(self, subject = "Missed subject", message = "Empty message", credentials_filename="credentials.json"):
        credentials_data = {}

        with open(credentials_filename, "r") as json_file: 
            credentials_data = json.load(json_file)
                
        server = smtplib.SMTP(credentials_data["smtp_server"], int(credentials_data["smtp_port"]))
        server.starttls()
        server.login(credentials_data["email_login"], credentials_data["email_password"])
        message = """Subject: {0}

                    Message:
                    {1}
                    """.format(subject, message)
        server.sendmail(credentials_data["sender_email"], credentials_data["receivers"], message)
        server.quit()
        pass


if __name__ == "__main__":

    previous_links = []
    # tbsWroclawForRentPage = WebPageMock("/home/kamilsiecinski/Pulpit/Projects/webpagechangenotifier/index.html")
    tbsWroclawForRentPage = WebPage("http://www.tbs-wroclaw.com.pl/mieszkania-na-wynajem/")
    tbsForRentParser = WebPageParser()

    while True:
        html_correct = False
        while html_correct is False:
            html = tbsWroclawForRentPage.get_html_code()
            if "429 Too Many Requests" in tbsForRentParser.get_web_title(html):
                time.sleep(20)
            else: 
                html_correct = True
        http_links = tbsForRentParser.get_http_links(html)
        # Remove all links containing .pdf extension"
        http_links = [link for link in http_links if ".pdf" not in link]
        # Remove these ones which does not have html extension
        http_links = [link for link in http_links if ".html" in link]

        new_links = [link for link in http_links]
        for old_link in previous_links:
            if old_link in new_links:
                new_links.remove(old_link)
        previous_links = http_links
        for link in new_links :

            timestamp_str = datetime.datetime.now().strftime("%d_%b_%Y_%H_%M_%S__")
            filename = link.split("/")[-1] 
            filename = timestamp_str + filename
            appartment_page = WebPage(link)
            appartment_html = appartment_page.get_html_code()
            html_correct = False
            while html_correct is False:
                appartment_html = appartment_page.get_html_code()
                if "429 Too Many Requests" in WebPageParser().get_web_title(appartment_html):
                    time.sleep(5)
                else: 
                    html_correct = True

            with open(filename, "w") as file: 
                file.write(appartment_html)

            subject = "TBS Wroclaw {0}".format(link.split("/")[-1].split(".html")[0])
            message = """
            New appartment probably appeared:
            Please visit this pages:
            {0}
            """.format("\r\n".join(new_links))
            print(subject)
            EmailNotifier().sendNotification(subject = subject, message = message, credentials_filename="credentials.json")